package com.example.pjsipsample.sip

import android.util.Log
import org.pjsip.pjsua2.*


private const val TAG = "SIP_APP"
private const val LOG_LEVEL = 4L

object SipApp {
    var listener: SipAppListener? = null
    var isInitialized = false
    private val epConfig = EpConfig()
    private val sipTpConfig = TransportConfig()
    private lateinit var endpoint: Endpoint

    private fun loadLibrary() {
        System.loadLibrary("pjsua2")
        Log.i(TAG, "Library loaded")
    }

    fun initialize(connectionInfo: SipConnectionInfo, listener: SipAppListener?=null) {
        if(!isInitialized){
            loadLibrary()
            endpoint = Endpoint()
            Log.i(TAG, "SipApp.initialize(): Initializing SIP App on thread with connection => $connectionInfo")
            this.listener = listener

            /* Create endpoint */
            try {
                Log.i(TAG, "SipApp.initialize(): Creating SIP Endpoint")
                endpoint.libCreate()
                Log.d(TAG, "SipApp.initialize(): SIP Endpoint created " + Thread.currentThread().name)
            } catch (e: java.lang.Exception) {
                Log.e(TAG, "SipApp.initialize(): Could not create SIP Endpoint due to: ", e)
                this.listener?.notifyDidFinishStaring(false)
                return
            }

            /* Set 'default' values */
            sipTpConfig.port = connectionInfo.sipDevicePort //random generated

            /* Override log level setting */
            epConfig.logConfig.level = LOG_LEVEL
            epConfig.logConfig.consoleLevel = LOG_LEVEL

            /* Set log config. */

            /* Set log config. */
            val logCfg: LogConfig = epConfig.logConfig
            logCfg.writer =  SipLogWriter()
            logCfg.decor = logCfg.decor and (pj_log_decoration.PJ_LOG_HAS_CR or pj_log_decoration.PJ_LOG_HAS_NEWLINE).inv().toLong()

            /* Set ua config. */

            /* Set ua config. */Log.d(
                TAG,
                "SipApp.initialize(): Setting up SIP User Agent " + Thread.currentThread().name
            )
            val uaCfg: UaConfig = epConfig.uaConfig
            uaCfg.maxCalls = 1
            uaCfg.userAgent = "Sip Android"
            val outboundProxies = StringVector()
            outboundProxies.add("sip:${connectionInfo.sipHost}:${connectionInfo.sipPort};transport=tcp")
            uaCfg.outboundProxies = outboundProxies
            // Added stun server
            val stunServers = StringVector()
            stunServers.add("stun.l.google.com:19302")
            stunServers.add("stun1.l.google.com:19302")
            stunServers.add("stun2.l.google.com:19302")
            stunServers.add("stun3.l.google.com:19302")
            stunServers.add("stun4.l.google.com:19302")
            stunServers.add("stun.mangovoice.com:19302")
            uaCfg.stunServer = stunServers

            /* Init endpoint */
            try {
                Log.d(TAG, "SipApp.initialize(): Initializing SIP Endpoint")
                endpoint.libInit(epConfig)
                Log.d(TAG, "SipApp.initialize(): SIP Endpoint Initialized")
            } catch (e: java.lang.Exception) {
                Log.e(TAG, "Error while initializing PJSIP JNI Library", e)
                this.listener?.notifyDidFinishStaring(false)
                return
            }

            /* Create transports. */
            try {
                Log.d(TAG, "SipApp.initialize(): Creating SIP Endpoint Transport " + Thread.currentThread().name)
                endpoint.transportCreate(pjsip_transport_type_e.PJSIP_TRANSPORT_TCP, sipTpConfig)
                Log.d(TAG, "SipApp.initialize(): SIP Endpoint Transport Created " + Thread.currentThread().name)
            } catch (e: java.lang.Exception) {
                Log.e(TAG, "Error while creating Transport", e)
                this.listener?.notifyDidFinishStaring(false)
                return
            }

            try {
                val priorities: HashMap<String, Short?> = HashMap()
                priorities["opus/48000/2"] = Integer.valueOf(130).toShort()
                priorities["PCMA/8000/1"] = Integer.valueOf(129).toShort()
                priorities["PCMU/8000/1"] = Integer.valueOf(128).toShort()
                priorities["G722/16000/1"] = Integer.valueOf(127).toShort()
                priorities["speex/32000/1"] = Integer.valueOf(0).toShort()
                priorities["speex/16000/1"] = Integer.valueOf(0).toShort()
                priorities["speex/8000/1"] = Integer.valueOf(0).toShort()
                priorities["iLBC/8000/1"] = Integer.valueOf(0).toShort()
                priorities["GSM/8000/1"] = Integer.valueOf(0).toShort()
                val codecs: CodecInfoVector2 = endpoint.codecEnum2()
                for (i in codecs.indices) {
                    val info = codecs[i]
                    val codecId = info.codecId
                    val priority = if (priorities[codecId] != null) priorities[codecId]?:0 else 0
                    endpoint.codecSetPriority(codecId, priority)
                }
            } catch (ex: java.lang.Exception) {
                Log.e(TAG, "Could not load codecs into endpoint", ex)
                this.listener?.notifyDidFinishStaring(false)
                return
            }

            /* Start. */

            /* Start. */try {
                Log.d(TAG, "SipApp.initialize(): Starting SIP Endpoint " + Thread.currentThread().name)
                endpoint.libStart()
                Log.d(TAG, "SipApp.initialize(): SIP Endpoint Started " + Thread.currentThread().name)
            } catch (e: java.lang.Exception) {
                Log.e(TAG, "Error while starting SIP Endpoint " + Thread.currentThread().name, e)
                this.listener?.notifyDidFinishStaring(false)
                return
            }
            isInitialized = true
        }
    }

    fun finish(){
        if(isInitialized){
            try {
                Log.i(TAG, "SipApp.finish(): Destroying library")
                endpoint.libDestroy()
                Log.i(TAG, "SipApp.finish(): Library destroyed")
            } catch (e: Exception) {
                Log.e(TAG, "Error while destroying library", e)
            }
        }
    }

    interface SipAppListener {
        fun notifyRegState(code: Int, reason: String?, expiration: Int)
        fun notifyIncomingCall(call: Call?)
        fun notifyCallState(sipCall: Call)
        fun notifyCallMediaState(call: Call?)
        fun onCallTransferStatus(prm: OnCallTransferStatusParam?)
        fun onNatDetectionComplete(prm: OnNatDetectionCompleteParam?)
        fun onMwiInfo(prm: OnMwiInfoParam?)
        fun notifyDidFinishStaring(success: Boolean)
        fun onIpChangeProgress(prm: OnIpChangeProgressParam?)
    }
}

data class SipConnectionInfo( var displayName: String,  var domain: String, var username: String,
                          var password: String, var sipHost: String, var sipPort: Int,
                          var sipDevicePort: Long = -1, var stunServer: String)

class SipLogWriter: LogWriter(){
    override fun write(entry: LogEntry) {
        Log.d(TAG, entry.msg)
    }
}