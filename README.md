## Instructions to build libraries
1. Download & Install [Docker](https://www.docker.com/products/docker-desktop/)
2. Create a folder with the name `builder-output` in the project root
3. Build the docker image with `docker build --no-cache -t pjsip-builder ./builder/`
4. Run the docker container with `docker run --name pjsip-builder -v $(pwd)/builder-output:/pjsip_builder/output -ti pjsip-builder`
5. Unzip files and copy them to sample proyect
6. Add to your shell session (~/.bashrc or ~/.bash_profile or ~/.zshrc ) file the following env variables with the corresponding values

```
export DISPLAY_NAME=XXXXXXXX
export USERNAME=XXXXXX
export PASSWORD=XXXXXX
export SIP_HOST=wwww.XXXXX.com
export SIP_PORT=XXXX
export SIP_DEVICE_PORT=XXXXX
export STUN_SERVER=XXXXXX
```

